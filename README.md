# Install Script

## 1. Introductions

This repository includes the installation scripts required for variable aspects of development, **these are for  DEBIAN/UBUNTU distribution only, unless otherwise specified**, these will be divided into their individual areas:

 1. Cloud Services
 2. Continuous Integration and Deployment (CI-CD)
 3. Scripting
 4. Images
 5. Project specific

Please navigate to the correct folder of the service you wish to install. Plese ensure that you read the readme in each folder as they will provide insight into each script where appropriate, this is particularly important where an additional configuration file is required for deployment.

