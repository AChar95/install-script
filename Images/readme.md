# Images

## 1. Introduction

Here there are scripts that are used to install docker, and maven. These scripts will ensure, the system in use has the required additional featuressuch as java.

## 2. Caution

Before installing maven using the 

Before installing docker using the docker.sh script, please be aware that this script will essentially give docker root permission, this is to mitigate the requirement of entering sudo before each command. If you _do not_ wish to give docker this ability then simply use the file **docker-without-sudo.sh** this will install docker but not give it any root permissions.
